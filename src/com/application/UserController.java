package com.application;


import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Path("/")
public class UserController {

	@Context
	HttpServletRequest request;

	@GET
	@Path("/")
	@Produces(MediaType.TEXT_HTML)
	public Response loginPage() {
		return Response.ok(new Viewable("/Login.jsp")).build();
	}

	@GET
	@Path("/signUp")
	@Produces(MediaType.TEXT_HTML)
	public Response signUpPage() {
		return Response.ok(new Viewable("/Signup.jsp")).build();
	}

	@GET
	@Path("/showLocation")
	@Produces(MediaType.TEXT_HTML)
	public Response showLocationPage() {
		return Response.ok(new Viewable("/ShowLocation.jsp")).build();
	}
	
	@GET
	@Path("/getFollowingForm")
	@Produces(MediaType.TEXT_HTML)
	public Response showfollowingPage() {
		return Response.ok(new Viewable("/followForm.jsp")).build();
	}

	@GET
	@Path("/unfollow")
	@Produces(MediaType.TEXT_HTML)
	public Response unfollow() {
		return Response.ok(new Viewable("/unfollow.jsp")).build();
	}
	@GET
	@Path("/home")
	@Produces(MediaType.TEXT_HTML)
	public Response home() {
		return Response.ok(new Viewable("/home.jsp")).build();
	}

	@GET
	@Path("/followUser")
	@Produces(MediaType.TEXT_HTML)
	public Response followUser() {
		return Response.ok(new Viewable("/followUser.jsp")).build();
	}
	
	@GET
	@Path("/profile")
	@Produces(MediaType.TEXT_HTML)
	public Response profile() {
		return Response.ok(new Viewable("/profile.jsp")).build();
	}

	@GET
	@Path("/getFollowing")
	@Produces(MediaType.TEXT_HTML)
	public Response getfollowers() {
		return Response.ok(new Viewable("/getFollowing.jsp")).build();
	}

	@GET
	@Path("/search")
	@Produces(MediaType.TEXT_HTML)
	public Response search() { // I am not sure about if it go to home ! how can
								// IT identify search
		return Response.ok(new Viewable("/home.jsp")).build();
	}

	/************************ Update Location *****************/
	@POST
	@Path("/updateMyLocation")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateLocation(@FormParam("lat") String lat,
			@FormParam("long") String lon) {
		HttpSession session = request.getSession();
		Long id = (Long) session.getAttribute("id");
		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/updatePosition";
		//String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/updatePosition";

		String urlParameters = "id=" + id + "&lat=" + lat + "&long=" + lon;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONParser parser = new JSONParser();
		JSONObject obj;
		try {
			obj = (JSONObject) parser.parse(retJson);
			Long status = (Long) obj.get("status");
			if (status == 1)
				return "Your location is updated";
			else
				return "A problem occured";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "A problem occured";

	}
/********************** Show Current Location***********/
	@POST
	@Path("/showLocation")
	@Produces(MediaType.TEXT_HTML)
	public Response showLocation() {
		HttpSession session = request.getSession();
		String userId = (String)session.getAttribute("id");
		String urlParameters = "id=" + userId;
		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/userCurrentLoscation";
		//String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/userCurrentLoscation";
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			// // list of names ?!!!!!!!!!!!!!!
			obj = (JSONObject) parser.parse(retJson);
			Map<String, String> map = new HashMap<String, String>();
			map.put("long", (String) obj.get("long"));
			map.put("lat", (String) obj.get("lat"));

			return Response.ok(new Viewable("/ShowLocation.jsp", map)).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/*************************** FollowingList ************************/
	@GET
	@Path("/followingList")
	@Produces(MediaType.TEXT_HTML)
	public Response followingList() {
		
		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/followingList";	
//		String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/getfollowers";
		HttpSession session = request.getSession();
		String userId = session.getAttribute("id").toString();
		String urlParameters = "id=" + userId;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
		JSONParser parser = new JSONParser();
		try {
		System.out.println("hello");
		JSONArray obj = (JSONArray) parser.parse(retJson);
			Map<String, Vector<Map <String, String> > >  users = new HashMap<String, Vector<Map <String, String> > >();
			Vector<Map <String, String> > followerBack = new Vector<Map<String,String> >();
			System.out.println("hello2");
		System.out.println("dfgdf"+obj.size());
			for (int i = 0; i < obj.size(); i++) {
				JSONObject ob = (JSONObject)obj.get(i);
				
				Map <String, String>map = new HashMap();
				//System.out.println(obj.get();
				map.put("email",ob.get("email").toString());
				map.put("name", ob.get("name").toString());
				map.put("id",  ob.get("id").toString());
				System.out.println(map.get("name"));
				followerBack.add(map);
			}
			users.put("followingList",followerBack);
			// map.put("email", (String) obj.get("email"));
			return Response.ok(new Viewable("/getFollowing.jsp", users)).build();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/********************************** search *********************/
	@POST
	@Path("/doSearch")
	@Produces(MediaType.TEXT_HTML)
	public Response showSearchList(@FormParam("name") String name) {

		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/search";		
		//String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/search";
		String urlParameters = "name=" + name;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		// HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			// // list of names ?!!!!!!!!!!!!!!
			obj = (JSONObject) parser.parse(retJson);
			Map<String, String> map = new HashMap<String, String>();
			ArrayList<Map> followerBack = new ArrayList<Map>();
			// Is this true ?!
			@SuppressWarnings("unchecked")
			ArrayList<String> searchName = (ArrayList<String>) obj
					.get("search");

			for (int i = 0; i < searchName.size(); i++) {
				String[] spltArray = searchName.get(i).split(",");
				map.put("email", spltArray[0]);
				map.put("name", spltArray[1]);
				map.put("lat", spltArray[2]);
				map.put("long", spltArray[3]);
				map.put("id", spltArray[4]);
				followerBack.add(map);
			}
			// map.put("email", (String) obj.get("email"));
			return Response.ok(new Viewable("/searchResult.jsp", followerBack)).build();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/********************** Follow User ***********************/
	@POST
	@Path("/doFollow")
	/************** follow in the same page with unfollow****/
	@Produces(MediaType.TEXT_HTML)
	public Response  showFollow(@FormParam("email") String email) {
		//System.out.println("hrerer------");
		HttpSession session = request.getSession();
		//String userId = (String)session.getAttribute("id");
		// how can I get followed Id ?
		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/followUser";
//		String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/followUser";
		String urlParameters = "userId=" + session.getAttribute("id")+"&followedEmail="+email/* +"&followedId="+followedId */;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		// return what ?!
		/*JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			Map<String, String> map = new HashMap<String, String>();

			map.put("follow", (String) obj.get("isfollowed"));
			return Response.ok(new Viewable("/profile.jsp", map)).build();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;*/
		//return Response.ok(new Viewable("/Login.jsp")).build();
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", (String) session.getAttribute("name"));
		map.put("email", (String) session.getAttribute("email"));

		return Response.ok(new Viewable("/home.jsp", map)).build();

	}
	/********************** unFollow User ***********************/
	@POST
	@Path("/doUnFollow")
	/******************************** Unfollow **************************/
	@Produces(MediaType.TEXT_HTML)
	public Response showUnfollow(@FormParam("email") String email ) {
		HttpSession session = request.getSession();
		//String userId = (String) session.getAttribute("id");
		// how can I get followed Id ?

		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/unfollow";
		//String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/unfollow";
		String urlParameters = "userId=" + session.getAttribute("id")+"&followedEmail="+email/* +"&followedId="+followedId */;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		// return what ?!
		/*JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			Map<String, String> map = new HashMap<String, String>();

			map.put("unfollow", (String) obj.get("unfollowed"));
			return Response.ok(new Viewable("/profile.jsp", map)).build();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", (String) session.getAttribute("name"));
		map.put("email", (String) session.getAttribute("email"));

		return Response.ok(new Viewable("/home.jsp", map)).build();
		//return null;

	}
	@POST
	@Path("/doLogin")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("email") String email,
			@FormParam("pass") String pass) {
		// String serviceUrl =
		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/login";
		//String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/login";

		String urlParameters = "email=" + email + "&pass=" + pass;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("email", obj.get("email"));
			session.setAttribute("name", obj.get("name"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("name"));
			map.put("email", (String) obj.get("email"));

			return Response.ok(new Viewable("/home.jsp", map)).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@POST
	@Path("/doSignUp")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("name") String name,
			@FormParam("email") String email, @FormParam("pass") String pass) {
		// String serviceUrl =
		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/signup";
		//String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/signup";

		String urlParameters = "name=" + name + "&email=" + email + "&pass="
				+ pass;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONParser parser = new JSONParser();
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("email", obj.get("email"));
			session.setAttribute("name", obj.get("name"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("name"));
			map.put("email", (String) obj.get("email"));

			return Response.ok(new Viewable("/home.jsp", map)).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}