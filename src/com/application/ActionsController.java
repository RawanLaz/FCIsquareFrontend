package com.application;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


@Path("/")
public class ActionsController {

	
	@Context
	HttpServletRequest request;
	
	
	@GET
	@Path("/showUserHistory")
	@Produces(MediaType.TEXT_HTML)
	public Response showHistory() {
		
//		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/getUserActions";	
		String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/getUserActions";
		HttpSession session = request.getSession();
		String userId = session.getAttribute("id").toString();
		String urlParameters = "userId=" + userId;
		//System.out.println(userId);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		// and where it will pass parameter
		
		JSONParser parser = new JSONParser();
	
		try {
			
		//System.out.println("hello");
		JSONArray obj = (JSONArray) parser.parse(retJson);
			Map<String, Vector<Map <String, String> > >  actions = new HashMap<String, Vector<Map <String, String> > >();
			//followinglist , array of all users , map is data of one user for example 
			Vector<Map <String, String> > actionback = new Vector<Map<String,String> >();

			//System.out.println("hello2");
		System.out.println("dfgdf"+obj.size());
			for (int i = 0; i < obj.size(); i++) {
				JSONObject ob = (JSONObject)obj.get(i);//get one user
				
				Map <String, String>map = new HashMap<>();
				//System.out.println(obj.get();
				map.put("id",ob.get("id").toString());
				map.put("userId", ob.get("userId").toString());
				map.put("actionId", ob.get("actionId").toString());
				map.put("undoURL",  ob.get("undoURL").toString());
				map.put("time",  ob.get("time").toString());
				map.put("descretption",  ob.get("descreption").toString());
				System.out.println(map.get("time"));
				actionback.add(map);
			}
			actions.put("actionsList",actionback);
			// map.put("email", (String) obj.get("email"));
			return Response.ok(new Viewable("/showHistory.jsp", actions)).build();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@POST
	@Path("/undoHistory")
	@Produces(MediaType.TEXT_HTML)
	public Response undoHistory(@FormParam("actionId") String id,
			@FormParam("undoURL") String undoService)
	{
//		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/";	
		String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/";
		serviceUrl+=undoService;
		System.out.println(serviceUrl);
		HttpSession session = request.getSession();
		String userId = session.getAttribute("id").toString();
		String urlParameters = "userId=" + userId +"&actionId="+id;
		System.out.println(id);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", (String) session.getAttribute("name"));
		map.put("email", (String) session.getAttribute("email"));

		return Response.ok(new Viewable("/home.jsp", map)).build();
	}
}
