package com.application;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Path("/")


public class PlaceContoller 
{
	@Context
	HttpServletRequest request;
	
	@GET
	@Path("/addNewPlace")
	@Produces(MediaType.TEXT_HTML)
	public Response addPlace() {
		return Response.ok(new Viewable("/addNewPlace.jsp")).build();
	}
	
	@POST
	@Path("/doAddNewPlace")
	@Produces(MediaType.TEXT_HTML)
	public Response addNewPlace(@FormParam("lat") String lat,
			@FormParam("long") String lon,@FormParam("name") String name,
			@FormParam("descreption") String desc)
	{
		HttpSession session = request.getSession();
		String id = session.getAttribute("id").toString();
		
		if(lon.equals("")||lat.equals(""))
		{
			lat = String.valueOf( session.getAttribute("lat"));
			lon =  String.valueOf( session.getAttribute("long"));
			System.out.println(lon+" lklkl "+lat);
		}
//		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/addNewPlace";
		
		String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/addNewPlace";
		
		
		String urlParameters = "id=" + id+"&name=" + name+ "&lat=" + lat + "&lon=" + lon
				+"&descreption=" + desc;
		 System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONParser parser = new JSONParser();
		JSONObject obj;
		try {
			obj = (JSONObject) parser.parse(retJson);
			Long status = (Long) obj.get("status");
			if (status == 1)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", (String) session.getAttribute("name"));
				map.put("email", (String) session.getAttribute("email"));
	
				return Response.ok(new Viewable("/home.jsp", map)).build();
			}
			else
			{
				return Response.ok(new Viewable("/addNewPlace.jsp")).build();
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.ok(new Viewable("/addNewPlace.jsp")).build();
		

	}
}
