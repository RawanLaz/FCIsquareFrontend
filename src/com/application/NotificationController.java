package com.application;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class NotificationController
 */
@Path("/")
public class NotificationController  {
	@Context
    HttpServletRequest request;

	@GET
	@Path("/getNotifications")
	@Produces(MediaType.TEXT_HTML)
	public Response getNotifications() {
//		String serviceUrl = "http://fcisquarebackend-csrashe.rhcloud.com/FCIsquareBackend/rest/getNotifications";	
		String serviceUrl = "http://localhost:8080/FCIsquareBackend/rest/getNotifications";
		System.out.println("hello :v");
		HttpSession session = request.getSession();
		String userId = session.getAttribute("id").toString();
		String urlParameters = "id=" + userId;
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		
		JSONParser parser = new JSONParser();
		try {
		JSONArray obj = (JSONArray) parser.parse(retJson);
		Map<String, Vector<Map <String, String> > >  notis = new HashMap<String, Vector<Map <String, String> > >();
		Vector<Map <String, String> > noti = new Vector<Map<String,String> >();
			for (int i = 0; i < obj.size(); i++) {
				JSONObject ob = (JSONObject)obj.get(i);
				Map <String, String>map = new HashMap<>();
				map.put("notificationId",ob.get("notificationId").toString());
				map.put("discription", ob.get("discription").toString());
				map.put("date",  ob.get("date").toString());
				map.put("senderName",  ob.get("senderName").toString());
				map.put("linkParameter",ob.get("linkParameter").toString());
				
				System.out.println("linkParameter"+map.get("linkParameter"));
				noti.add(map);
			}
			notis.put("getNotifications",noti);
			return Response.ok(new Viewable("/getNotifications.jsp", notis)).build();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@POST
	@Path("/getNotificationSource")
	@Produces(MediaType.TEXT_HTML)
	public Response getNotificationSource(@FormParam("id") String id) {
		if(id.contains("Like")||id.contains("like")||id.contains("Comment")||id.contains("comment")){
			CheckInController cic=new CheckInController();
//			System.out.println("the id : "+id);
			String theidonly=id.substring(id.indexOf("_")+1);
//			System.out.println("the id : "+id+" the id only : "+theidonly);
			return cic.ShowCheck_In(theidonly);
		}else {
			System.out.println("no such path!");
		}
		
		return null;
	}

}