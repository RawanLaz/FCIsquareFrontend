<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>FCISquare User History</title>



<!DOCTYPE html>
<html>
<head>
<style>
table { 
color: #333;
font-family: Helvetica, Arial, sans-serif;
width: 640px; 
border-collapse: 
collapse; border-spacing: 0; 
}

td, th { 
border: 1px solid transparent;
height: 30px; 
transition: all 0.5s; 
}

th {
background: #6EB5C0; 
font-weight: bold;

}

td {
background: #FAFAFA;
text-align: center;
}


tr:nth-child td { background: #6EB5C0; }   

tr td:hover { background: #FFCCBB; color: #006C84; } 


</style>
</head>
<body>
<table id="table">
 <tr>
    <th>Your history</th>
  </tr>
  
<c:forEach items = "${it.actionsList}" var ="act"> 
  <tr>
  <form action = "/FCISquareFrontend/app/undoHistory"  method = "post" >
    <td><p><c:out value="${act.descretption} at time ${act.time}"></c:out> </p></td>
     <input type="hidden" name="actionId" value=${act.actionId} />
     
     <input type="hidden" name="undoURL" value=${act.undoURL} />
      <td><p><input type="submit" value="Undo"></p></td>
      </form>
  </tr>
</c:forEach>
</table>
</body>
</html>



