<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<style>
table { 
color: #333;
font-family: Helvetica, Arial, sans-serif;
width: 640px; 
border-collapse: 
collapse; border-spacing: 0; 
}

td, th { 
border: 1px solid transparent;
height: 30px; 
transition: all 0.5s; 
}

th {
background: #6EB5C0; 
font-weight: bold;

}

td {
background: #FAFAFA;
text-align: center;
}


tr:nth-child td { background: #6EB5C0; }   

tr td:hover { background: #FFCCBB; color: #006C84; } 


</style>

<body>

  <button onclick="myFunction()">Search</button>
<p id="demo"></p>

<script>
function myFunction() {
    var text = "";
    var i;
    for (i = 0; i < 5; i++) {
        text += "The number is " + i + "<br>";
    }
    document.getElementById("demo").innerHTML = text;
}
</script>

</body>






</html>
