<%-- <%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>FCISquare Get Following</title>


<style>
#table {
	color: #333;
	font-family: Helvetica, Arial, sans-serif;
	width: 640px;
	border-collapse: collapse;
	border-spacing: 0;
}

td, th, a, div {
	border: 1px solid transparent;
	height: 30px;
	transition: all 0.5s;
}

th, a, div {
	background: #6EB5C0;
	font-weight: bold;
}

td, a, div {
	background: #FAFAFA;
	text-align: center;
}

tr:nth-child td, a:nth-child, div:nth-child {
	background: #6EB5C0;
}

tr td:hover, a:hover, div:hover {
	background: #FFCCBB;
	color: #006C84;
}
</style>
</head>
<body>
	<table id="table">
		<tr>
			<th>Notifications:</th>
		</tr>

		<c:forEach items="${ it.getNotifications}" var="noti">
			<div id="nav">
				<a href="/FCISquareFrontend/app/home" style="display: block;"> 
				<c:out value="   ${noti.senderName}  |   ${noti.discription}  |  ${noti.date}"></c:out>
				</a>
			 </div>
			 
			<tr id="nav">
				<td><a href="/FCISquareFrontend/app/getCheckIn?id="> <c:out
							value="${noti.senderName} ${noti.linkParameter}"></c:out>
				</a></td>
				<td><a href="/FCISquareFrontend/app/getCheckIn"> <c:out
							value="${noti.discription}"></c:out>
				</a></td>
				<td><a href="/FCISquareFrontend/app/getCheckIn"> <c:out
							value="${noti.date}"></c:out>
				</a></td>
			</tr>
		</c:forEach>
	</table>
	<ul>
		<c:forEach items="${ it.getNotifications}" var="noti">
			<c:out value="${noti.discription}"></c:out><br>
		</c:forEach>
	</ul>
</body>
</html> --%>
 
 <%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>FCISquare Get Following</title>


<style>
table {
	color: #333;
	font-family: Helvetica, Arial, sans-serif;
	width: 640px;
	border-collapse: collapse;
	border-spacing: 0;
}

td, th, a, div {
	border: 1px solid transparent;
	height: 30px;
	transition: all 0.5s;
}

th {
	background: #6EB5C0;
	font-weight: bold;
}

td {
	background: #FAFAFA;
	text-align: center;
}

tr:nth-child td {
	background: #6EB5C0;
}

tr td:hover {
	background: #FFCCBB;
	color: #006C84;
}
</style>
</head>
<body>
	<table id="table">
		<tr>
			<th>Notifications:</th>
		</tr>

		<c:forEach items="${ it.getNotifications}" var="noti">
			<tr>
				<form action="/FCISquareFrontend/app/getNotificationSource" method="post">
					<td>
						<p><c:out value="${noti.discription}  ${noti.date}"></c:out></p>
					</td>
					<input type="hidden" name="id" value=${noti.linkParameter} /> 
					<td><p><input type="submit" value="Show"></p></td>
				</form>
			</tr>
		</c:forEach>
		<c:forEach items="${ it.getNotifications}" var="noti">
			<div id="nav">
				<a href="/FCISquareFrontend/app/getNotificationSource" style="display: block;">
					<c:out
						value="  ${noti.senderName}  |  ${noti.discription}  ${noti.date}"></c:out>
				</a>

			</div>
		</c:forEach>
	</table>
</body>
</html>




 