<%@ page language="java" contentType="text/html; charset=windows-1256"
	pageEncoding="windows-1256"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1256">
<title>FCISquare Get Following</title>



<!DOCTYPE html>
<html>
<head>
<style>
table { 
color: #333;
font-family: Helvetica, Arial, sans-serif;
width: 640px; 
border-collapse: 
collapse; border-spacing: 0; 
}

td, th { 
border: 1px solid transparent;
height: 30px; 
transition: all 0.5s; 
}

th {
background: #6EB5C0; 
font-weight: bold;

}

td {
background: #FAFAFA;
text-align: center;
}


tr:nth-child td { background: #6EB5C0; }   

tr td:hover { background: #FFCCBB; color: #006C84; } 


</style>
</head>
<body>
<table id="table">
 <tr>
    <th>You Are Following</th>
  </tr>
  
<c:forEach items = "${ it.followingList}" var ="user"> 
  <tr>
    <td><p>User name :<c:out value="${user.name}"></c:out> </p></td>
      <td><p>User Email :<c:out value="${user.email}"></c:out> </p></td>
  </tr>
</c:forEach>
</table>
</body>
</html>



